# Cerebrum

Static Memory Module for DiRAD Logging Structure

### Add to your code

This is not added as a usual npm module as it is not listed on npm. The code is stored publicly in our bitBucket. 

To add the code to your project add the folowing to your dependencies in your package.json file. 

```json
{
"cerebrum": "git+https://bitbucket.org/diradtechnologies/nm-cerebrum.git"
}
```

It can then be used as a normal module at the top of your code.

```javascript
const cerebrum = require('cerebrum')
```

## Usage

### init

Use the init function to configure the database connection. Cerebrum uses mysql cluster pools to connect to mySQL databases.

```javascript
const cerebrum = require('cerebrum')

var options = {
    "servers": {
        "node1": {
            "connectionLimit": 10,
            "host": "example-server.dirad.cloud",
            "user": "user",
            "password": "*********",
            "database": "config"
        },
        "node2": {
            "connectionLimit": 10,
            "host": "example-server.dirad.cloud",
            "user": "user",
            "password": "*********",
            "database": "config"
        }
    },
    "clusterOptions": {
        "defaultSelector": "RR",
        "removeNodeErrorCount": "15",
        "restoreNodeTimeout": 30000
    }
}

cerebrum.init(dbOptions);

```

- servers -- list all of the nodes used in your cluster and the database to connect to. The object is passed stright into mySQL. Details on mySQL connection options can be found here: (https://www.npmjs.com/package/mysql#connection-options)

- clusterOptions -- mySQL cluster options sent directly to mySQL. Options can be found here: (https://www.npmjs.com/package/mysql#poolcluster-options)


### recall

Use recall to pull the must recent data from the database. This should be used during your apps start up. The functions are async and must be nested in an async function for the time being. Recall takes the table, type and enviroment. It will return a promise the returns the JSON object from the database.

```javascript
var config = await cerebrum.recall('proxy', 'config', '0');
```

### memorize

The memorize function will check for changes in the data for a particular table, type, environmnet combination. If there are changes it will right the updated changes to the database. Memorize requires the table, type, enviroment and the JSON object you want to remember. 

```javascript
var result = await cerebrum.memorize('proxy', 'config', '0', {"foo":"bar"});
```

### forget

The forget function will delete all the data from the config database for the specific type and enviroment according to the deletionMethod. Memorize requires the table, type, enviroment and method you wish to delete with.

- exponential - Will Maintain one record from each hour for a day, 1 per day for a week, and 1 per week forever.

- all - deletes everything but the last record added.

- default - keeps all data for 24 hours.

```javascript
var forget = await cerebrum.forget('proxy', 'config', '0', "all");
```


