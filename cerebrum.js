const mysql = require('mysql2');
const crypto = require('crypto')
var poolCluster
var pool
var lastBackup = {};


module.exports = {
    init: function (options) {
        poolCluster = mysql.createPoolCluster(options.clusterOptions);
        for (server of Object.keys(options.servers)) {
            poolCluster.add(server, options.servers[server]);
        }
        pool = poolCluster.of('node*')
    },
    recall: function (table, type, enviroment) {
        return new Promise((resolve, reject) => {
            try {
                pool.query("SELECT data FROM " + table + " WHERE TYPE = '" + type + "' AND enviroment = '" + enviroment + "' ORDER BY DATETIME DESC LIMIT 1", (error, results, fields) => {
                    if (error) {
                        reject(error)
                    } else {
                        if (Array.isArray(results) && results.length) {
                            lastBackup[table + '_' + type + '_' + enviroment] = results[0].data;
                            resolve(JSON.parse(results[0].data))
                        } else {
                            reject("No Results Found");
                        }
                    }
                })
            }
            catch (error) {
                reject(error)
            }
        })
    },
    memorize: function (table, type, enviroment, data) {
        return new Promise((resolve, reject) => {
            try {
                if (JSON.stringify(data) == lastBackup[table + '_' + type + '_' + enviroment]) {
                    resolve(true)
                } else {
                    lastBackup[table + '_' + type + '_' + enviroment] = JSON.stringify(data)
                    var queryString = "INSERT INTO " + table + "  VALUES(unhex('" + newID() + "'), '" + enviroment + "', '" + type + "', UTC_TIMESTAMP(), ?)"
                    pool.query(queryString, JSON.stringify(data), (error, results, fields) => {
                        if (error) {
                            reject(error)
                        } else {
                            resolve(true);
                        }
                    })
                }
            }
            catch (error) {
                reject(error)
            }
        })
    },
    forget: async function (table, type, enviroment, deletionMethod) {

        //return await getMostRecentRecord(table, enviroment, type);
        return new Promise(async (resolve, reject) => {
            try {
                var queryString = []
                var record = await getMostRecentRecord(table, enviroment, type).catch((err) => { reject(err) });
                record = (record) ? "AND id NOT in (unhex('" + record + "')) " : ''
                switch (deletionMethod) {
                    case "exponential":
                        queryString.push('CREATE TABLE temp' + table + '(id VARBINARY(16));')
                        queryString.push('INSERT INTO temp' + table + ' SELECT FIRST_VALUE(' + table + '.id) OVER(PARTITION BY hour(' + table + '.datetime) ORDER BY ' + table + '.datetime DESC) FROM ' + table + ' WHERE ' + table + '.type="' + type + '" AND DAY(' + table + '.datetime)=DAY(NOW()) AND MONTH(' + table + '.datetime)=MONTH(NOW()) GROUP BY hour(' + table + '.datetime),YEAR(' + table + '.datetime);')
                        queryString.push('INSERT INTO temp' + table + ' SELECT FIRST_VALUE(' + table + '.id) OVER(PARTITION BY day(' + table + '.datetime) ORDER BY ' + table + '.datetime DESC) FROM ' + table + ' WHERE ' + table + '.type="' + type + '" AND ' + table + '.datetime BETWEEN (NOW()-INTERVAL 1 week) AND NOW() GROUP BY day(' + table + '.datetime),YEAR(' + table + '.datetime);')
                        queryString.push('INSERT INTO temp' + table + ' SELECT FIRST_VALUE(' + table + '.id) OVER(PARTITION BY week(' + table + '.datetime) ORDER BY ' + table + '.datetime DESC) FROM ' + table + ' WHERE ' + table + '.type="' + type + '" GROUP BY week(' + table + '.datetime),YEAR(' + table + '.datetime);')
                        queryString.push("DELETE FROM " + table + " WHERE enviroment='" + enviroment + "' AND type='" + type + "' AND id NOT IN (select id from temp" + table + ")" + record + ";")
                        queryString.push('DROP TABLE temp' + table + ';')
                        break;
                    case "all":
                        queryString.push("delete FROM " + table + " WHERE TYPE='" + type + "' AND enviroment='" + enviroment + "' " + record)
                        break;
                    case "month":
                        queryString.push("delete FROM " + table + " WHERE TYPE='" + type + "' AND enviroment='" + enviroment + "' AND TIMESTAMPDIFF(HOUR, `datetime`, NOW()) > 168 " + record)
                        break;
                    default:
                        queryString.push("delete FROM " + table + " WHERE TYPE='" + type + "' AND enviroment='" + enviroment + "' AND TIMESTAMPDIFF(HOUR, `datetime`, NOW()) > 24 " + record)
                        break;
                }

                for (i in queryString) {
                    await new Promise((reso, rej) => {
                        pool.query(queryString[i], (error, results, fields) => {
                            if (error) {
                                rej(error);
                            } else {
                                reso(true);
                            }
                        })
                    })
                }
                resolve(true);
            }
            catch (error) {
                reject(error);
            }
        })
    }
}

function newID() {
    var hexstring = crypto.randomBytes(10).toString("hex");
    return [hexstring.substring(0, 19), new Date().getTime().toString()].join('');
}

async function getMostRecentRecord(table, enviroment, type) {
    return new Promise((resolve, reject) => {
        try {
            var queryString = "select hex(id) as id from " + table + " where type = '" + type + "' and enviroment = " + enviroment + " order by datetime desc limit 1"
            pool.query(queryString, (error, results, fields) => {
                if (error) {
                    reject(error)
                } else {
                    resolve(results[0].id);
                }
            })
        }
        catch (err) {
            reject(err)
        }
    })
}